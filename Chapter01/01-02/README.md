# 02. 자바 개발환경 설치하기

## 1. 자바 설치 하기
   
[자바 Open JDK 설치](https://jdk.java.net/)

[자바 Oracle JDK 설치](https://www.oracle.com/kr/java/technologies/downloads/)

[자바 JRE 설치](https://www.java.com/en/download/)

자바는 유료화 이후 JRE는 8.0까지만 무료 제공

## 2. 통합 개발 프로그램 설치하기

[eclipse 설치](https://www.eclipse.org/downloads/)

[IntelliJ 설치](https://www.jetbrains.com/ko-kr/idea/download/)

- 기존에 설치된 JDK가 없는 경우 

eclipse openJDK가 같이 설치 됨

![eclipse](./img/eclipse2.PNG)

IntelliJ 다운로드 함
        
![eclipse](./img/intelliJ.PNG)

- 무료버젼과 유료버젼을 확인하고 설치

- 계속적으로 업그레이드 됨

## 3. 자바 첫 프로그램 만들기


